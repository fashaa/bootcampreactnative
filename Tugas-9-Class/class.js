console.log(`\nNomor 1\n`)
console.log(`\nNomor 1 : Release 0\n`)

class Animal {
    constructor(name) {
        this.nama = name
        this._legs = 4
        this._cold_blooded = false
    }

    set legs(x){
        this._legs = x
    }

    set cold_blooded(x){
        this._cold_blooded = x
    }

    get name() {
        return this.nama
    }

    get legs() {
        return this._legs
    }

    get cold_blooded() {
        return this._cold_blooded
    }


}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log(`\nNomor 1 : Release 1\n`)

// Code class Ape dan class Frog di sini

class Ape extends Animal {
    constructor(name) {
        super(name)
        super.legs = 2

    }

    yell() {
        console.log("Auooo")
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name)
    }

    jump() {
        console.log("hop hop")
    }

}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 



console.log(`\nNomor 2\n`)

class Clock {
    constructor({ template }) {
        this.temp = template
        this.timer = null

    }

    get takeTemplate() {
        return this.temp
    }


    static render(test) {
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        var output = test.replace('h', hours).replace('m', mins).replace('s', secs);
        console.log(output);
    }

    stop() {
        clearInterval(this.timer);
    };

    start() {
        Clock.render(this.takeTemplate);
        this.timer = setInterval(Clock.render, 1000, this.takeTemplate);
    };

}

var clock = new Clock({ template: 'h:m:s' });
clock.start();