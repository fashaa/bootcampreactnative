console.log("========================SOAL NO.1========================")
function arrayToObject(arr) {
    var noitem = 0
    var now = new Date()
    var thisYear = now.getFullYear()
    arr.forEach(item => {
        
        var obj = {
            number : ++noitem,
            allname:`${item[0]} ${item[1]}`,
            dataperson : {
            firstname:item[0],
            lastname:item[1], 
            gender:item[2],
            age: item[3] <= thisYear ? Math.abs(thisYear-item[3]) : "Invalid Birth Year"}    
        }

        console.log(`${obj.number}. ${obj.allname} : `,obj.dataperson )
    });
}

var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
 
arrayToObject([])

console.log("\n")
console.log("========================SOAL NO.2========================")
var datashop =[
    {
        item : "Sepatu Stacattu",
        harga : 1500000
    },
    {
        item : "Baju Zoro",
        harga : 500000
    },
    {
        item :"Baju H&N",
        harga : 250000
    },
    {
        item :"Sweater Uniklo",
        harga :175000
    },
    {
        item : "Casing HP",
        harga :50000
    }
]
function shoppingTime(memberId, money) {
    var changemoney = money
    var listpurchased = []
    var error = {
        memberid : "",
        money : ""
    }
    datashop.forEach(item => {
        if (changemoney>=item.harga){
            listpurchased.push({
                itemName : item.item
            })
            changemoney-=item.harga
        

        }
    });

    var output = {
        memberid : ((memberId== "" || memberId==undefined)  ? error.memberid = "Mohon maaf, toko X hanya berlaku untuk member saja":memberId ),
        money : (money > datashop[datashop.length-1].harga ? money:error.money="Mohon maaf, uang tidak cukup"),
        listpurchased : listpurchased,
        changemoney : changemoney 
    }
    if (error.money =="" && error.memberid == ""){
        return output
    }else{
        return error.memberid=="" ? error.money:error.memberid
    }


}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime()); 
console.log("\n")



console.log("========================SOAL NO.3========================")
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    
    var output = []
    arrPenumpang.forEach(item => {
        if (rute.indexOf(item[1],0) > rute.indexOf(item[2],0)){
            rute = rute.reverse()
        }
        var kelipatan = rute.slice(rute.indexOf(item[1],0),rute.indexOf(item[2],0))
        output.push({
            penumpang : item[0],
            naikDari : item[1],
            tujuan : item[2],
            bayar : 2000 * kelipatan.length
        }
        )
    });

    
    
    return output
  }

  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B'],['2way', 'D', 'A']]));

  console.log(naikAngkot([]));