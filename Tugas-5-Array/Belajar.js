/*
Array adalah kumpulan atau tumpukan berbagai data.
Cara penulisan array :
- dengan menggunakan kurung siku []
- setiap elemen array memiliki indeks 0, 1,2,3
- dapat dimanipulasi (menambah, dan mengeluarkan elemen)
- dapat memasukan beberapa tipe data (salah satunya Array)

Array memiliki properti .length (panjang sebuah array)
Contoh :
var hobbies = ["sepeda", "berkuda","otomotif", "basket"]
console.log(hobbies)
console.loh(hobbies.length) -> outputnya 4

console.log(hobbies[0]) -> outputnya "Sepeda"

untuk mengakses elemen terakhir pada array
console.log(hobbies[-1])
*/

/*
=======METODE ARRAY=======
push -> menambahkan elemen pada index terakhir
- Contoh
var feeling = ["dag", "dig"]
feeling.push("dug") -> dug akan ditambahkan pada index terakhir Feeling

pop -> menghapus elemen pada index terakhir
feeling.pop() -> maka elemen terakhir "dug" dihapus

unshift -> menambah elemen pada index paling depan
var feeling =["dag", "dig"]
feeling.unshift("tur") -> "tur" akan ditambahkan pada index pertama

sort -> metode untuk mengurutkan nilai pada array (secara ascending) secara unicode
- Contoh
var binatang =["kera", "gajah", "musang"]
binatang.sort()
console.log(binatang) -> output : gajah, kera, musang

=====Tidak berlaku untuk angka=====
- Contoh 
var angka =[12,1,4]
angka.sort()
console.log(angka) -> outputnya 1,12,4 (dia melakukan sort pada karakter terdepan dari angka)

MAKA SOLUSINYA :
var angka =[12,1,4]
angka.sort(function (a,b) {retur a-b}) // nah ini akan urutan ascending. kalau descending urutan returnnya tinggal dibalik

slice -> adalah metode untuk mengambil irisan suatu array dengan memanggil INDEXNYA
var angka = [0,1,2,3,4]
var irisan1 = angka.slice(1,3)
console.log(irisan1) -> outputnya yaitu INDEX KE 1-3 yaitu (1,2,3)


splice -> metode untuk menghapus dan atau menambah nilai elemen pada array
strukturnya array.splice([index mulai],[Jumlah nilai yang ingin dihapus],[nilai yang ingin ditambahkan])
- CONTOH
var fruits =["banana","orange", "grape"]
fruits.splice(1,0,"watermelon") -> berarti menambahkan "watermelon"  di index 1
console.log(fruits) -> outputnya ["banana","watermelon","orange","grape"]

split dan join -> split : memecah sebuah string menjadi array
                  join  : menggabungkan array menjadi string

- CONTOH SPLIT
var biodata ="name : john, doe"
var name = biodata.split(":")
console.log(name) -> outputnya ["name", "john, doe"]

-CONTOH JOIN
var title = ["my", "name", "is", "Fasha"]
var slug = title.join("-") -> kata pada title akan dipisahkan dengan '-'
console.log(slug)
*/

var angka = [12,1,4]
angka.sort(function(b,a){return a-b})
console.log(angka)
console.log("\n")

var angka = [0,1,2,3,4]
var irisan1 = angka.slice(0,3)
console.log(irisan1)
console.log("\n")

var biodata ="name : john, doe"
var name = biodata.split(":")
console.log(name)
console.log("\n")

var title = ["my", "name", "is", "Fasha"]
var slug = title.join(" ")
console.log(slug)
console.log("\n")

console.log("#####TUGAS#####")


function range(startNum, finishNum){
    if (startNum <= finishNum){
        var x = []
        for (i =startNum ; i<=finishNum; i++){
            x.push(i)
        }
    }else if (startNum ==null && finishNum==null){
        return x=-1
    }
    else{
        var x =[];
        for (i=startNum; i>=finishNum; i--){
            x.push(i)
        }
        x.sort(function(startNum,finishNum) {return startNum<finishNum})
    }
    return x;
}
console.log(range(1,10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54,50))
console.log(range())

var biodata ="name : john, doe"
var name = biodata.split(":")
console.log(name)