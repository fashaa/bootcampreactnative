
console.log("#############SOAL No. 1#############")
function range(startNum, finishNum){
    var x = []
    if (startNum <= finishNum){ 
        for (i =startNum ; i<=finishNum; i++){
            x.push(i)
        }
    }else if (startNum ==null || finishNum==null){
        return x=-1
    }
    else{
        for (i=startNum; i>=finishNum; i--){
            x.push(i)
        }
        x.sort(function(startNum,finishNum) {return startNum<finishNum})
    }
    return x;
}
console.log(range(1,10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54,50))
console.log(range())
console.log("\n")

console.log("#############SOAL No. 2#############")
function rangeWithStep(startNum2, finishNum2, step){
    var y=[]
    if (startNum2<finishNum2){ 
        var i = startNum2
        while(i<finishNum2){
            y.push(i)
            i+=step
        }
        }
    else if (startNum2>finishNum2){
        var i =startNum2;
        while(i>finishNum2){
            y.push(i)
            i-=step
        }
    }
    
    return y;
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4))
console.log("\n")

console.log("#############SOAL No. 3#############")
function sum(startNum, finishNum, step){
    var rangeArr =[];
    var distance;

    if (!step){
        distance = 1
    } else {
        distance = step
    }

    if (startNum>finishNum){
        var currentNum = startNum;
        for (var i=0; currentNum>=finishNum; i++){
            rangeArr.push(currentNum)
            currentNum-=distance
        }
       
    }else if (startNum<finishNum){
        var currentNum = startNum;
        for (var i=0; currentNum<=finishNum; i++){
            rangeArr.push(currentNum)
            currentNum+=distance
        }
        
    }else if (!startNum && !finishNum && !step){
        return 0

    } else if (startNum){
        return startNum
    }
    var total = 0 ;
    for (var i=0; i<rangeArr.length; i++){
         total = total+rangeArr[i]
    }
    return total

}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log("\n")
console.log("#############SOAL No. 4#############")
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function dataHandling(data){
    var datalength = data.length
    for(var i=0; i<datalength; i++){
        var id = "nomor ID : "+data[i][0]
        var nama = "Nama lengkap : "+ data[i][1]
        var ttl = "TTL : "+ data[i][2] + " "+data [i][3]
        var hobi = "Hobi : "+data[i][4]

        console.log(id)
        console.log(nama)
        console.log(ttl)
        console.log(hobi)

    }
}
dataHandling(input)
console.log("\n")
console.log("#############SOAL No. 5#############")

function balikKata(str) {
    var currentString = str;
    var newString = '';
   for (let i = str.length - 1; i >= 0; i--) {
     newString = newString + currentString[i];
    }
    
    return newString;
   }
   console.log(balikKata("Kasur Rusak")) // kasuR rusaK
   console.log(balikKata("SanberCode")) // edoCrebnaS
   console.log(balikKata("Haji Ijah")) // hajI ijaH
   console.log(balikKata("racecar")) // racecar
   console.log(balikKata("I am Sanbers")) // srebnaS ma I 
   
   console.log("\n")
console.log("#############SOAL No. 6#############")

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input);

function dataHandling2(data){
    var newData = data
    var newName = data[1]+"Elsharawy"
    var newProv = "Provinsi" +data[2]
    var gender = "Pria"
    var institusi = "SMA Internasional Metro"

    newData.splice(1,1,newName)
    newData.splice(2,1,newProv)
    newData.splice(4,1,gender,institusi)

    var arrDate = data[3]
    var newDate = arrDate.split('/')
    var monthNum = newDate[1]
    var monthName = " "

    switch(monthNum){
        case "01" : monthName = "Januari"; break;
        case "02" : monthName = "Februari"; break;
        case "03" : monthName = "Maret"; break;
        case "04" : monthName = "April"; break;
        case "05" : monthName = "Mei"; break;
        case "06" : monthName = "Juni"; break;
        case "07" : monthName = "Juli"; break;
        case "08" : monthName = "Agustus"; break;
        case "09" : monthName = "September"; break;
        case "10" : monthName = "Oktober"; break;
        case "11" : monthName = "November"; break;
        case "12" : monthName = "Desember"; break;
        default : break;
    }
    var dateJoin = newDate.join("-")
    var dateArr = newDate.sort(function(value1,value2){
        value2-value1
    })
    var ediitName =newName.slice(0,15)
    console.log(newData)

    console.log(monthName)
    console.log(dateArr)
    console.log(dateJoin)
    console.log(ediitName)
}