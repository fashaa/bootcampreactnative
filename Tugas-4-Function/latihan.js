/* Function
Sebuah kode blok yang disusun untuk menjalankan suatu tindakan.
Strukturnya :
===========================================
function nama_function (parameter){
    [perintah/isi function]
    return [expression]
}
============================================
*/

//Contoh function tanpa return
function tampilkan(){
    console.log("Halo Gais !")
}
tampilkan()

console.log('===============================')
console.log('\n')

//Contoh function dengan return
function munculkanAngkaDua(){
    return 2
}
var tampung = munculkanAngkaDua() //dibuat variable dulu untu functionnya, bisa juga gausah
console.log(tampung)

console.log('===============================')
console.log('\n')

//Contoh function dengan parameter
function kali(angka){
    return angka*2 //operasi yang akan dijalankan

}
var tampung =kali(2)
console.log(tampung)

console.log('===============================')
console.log('\n')

//Contoh function dengan parameter lebih dari 1
function multi(x,y) { //disini parameter yang digunakan adalah x dan y
    return x * y //dilakukan operasi perkalian antara x dan y
}
console.log(multi(3,5)) //input yang akan dilakukan operasi perkalian tadi (x,y)

console.log('===============================')
console.log('\n')

// Inisialisasi parameter dengan nilai lebih dari satu

function tampil(angka=20){ //angka yang diassign adalah 20
    return angka
}
console.log(tampil(4))
console.log(tampil()) // jadi secara default akan menampilkan angka yang diassign pada function

console.log('===============================')
console.log('\n')

// Anonymous function, 
// yaitu kita dapat menampung function sebagai variable dengan sebuah bentuk function
var fungsiPerkalian = function(pertama,kedua){
    return pertama*kedua
}
console.log(fungsiPerkalian(19,87))