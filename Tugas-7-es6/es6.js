console.log("========================TUGAS NO.1========================")
const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
   
  golden()

  console.log("++++++++++++++++FUNGSI ARROW++++++++++++++++")

  goldenFunction = () =>{
      console.log('this Is GOLDEN !!')
  }
  goldenFunction()

  console.log("\n")
  console.log("========================TUGAS NO.2========================")
  const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
        return 
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 
  console.log("++++++++++++++++OBJECT LITERAL ES6++++++++++++++++")
  const firstName ='Wiliam'
  const lastName = 'Imoh'
 
  const theString = `${firstName} ${lastName}`

  console.log(theString)

  console.log("\n")
  console.log("========================TUGAS NO.3========================")
  const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  console.log("++++++++++++++++DESTRUCTING++++++++++++++++")
let student ={
    first: "Harry",
    last: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const {first, last, destination, occupation,spell} = student;
console.log(first, last, destination, occupation)

console.log("\n")
console.log("========================TUGAS NO.4========================")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)

console.log("++++++++++++++++ARRAY SPREADING++++++++++++++++")
let west2 = ["Will", "Chris", "Sam", "Holly"]
let east2 = ["Gill", "Brian", "Noel", "Maggie"]

let combined2= [...west2, ...east2]
console.log(combined2)

console.log("\n")
console.log("========================TUGAS NO.5========================")
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' +  
    'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
    'incididunt ut labore et dolore magna aliqua. Ut enim' +
    ' ad minim veniam'
 
// Driver Code
console.log(before)

console.log("++++++++++++++++TEMPLATE LITERALS++++++++++++++++")
const planet2 = "earth"
const view2 = "glass"
const after =`Lorem  ${view2} dolor sit amet consectetur adipiscing elit ${planet2} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

console.log(after)
