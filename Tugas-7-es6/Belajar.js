/* 
Es6 adalah bahasa pemrograman Javascript modern

Beberapa yang perlu dikuasai :

1. Let + Const
2. Arrow Function
3. Default Parameters
4. Destructring
5. Rest Parameters + Spread Operator
6. Enhanced Object Literals
7. Template Literals
8. Promises

*/

// Let + Const
// Normal Javascript
// var x =1;
// if (x==1){
//     var x =2
// console.log(x)
// }
// console.log(x)

//ES6
let x = 7;
 
if (x === 1) {
  let x =9;
 
  console.log(x);
 
}
 
console.log(x); 

const golden = function goldenFunction(){
    console.log("this is golden!!")
  }
   
  golden()

  console.log("++++++++++++++++")

  goldenFunction = () =>{
      console.log('this Is GOLDEN !!')
  }
  goldenFunction()